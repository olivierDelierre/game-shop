var express = require('express');
var app = express();

// Setting up body parser


let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Setting up mongoose.
let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/game-shop', { 
    useNewUrlParser: true,
    useUnifiedTopology: true
});

var db = mongoose.connection;

if (!db) {
    console.error('Error connecting db');
} else {
    console.log('Db connected');
}

app.get('/', (req, res) => res.send('Hello world!'));

// Routing
let apiRoutes = require('./routes/api-routes');

app.use('/api', apiRoutes);

app.listen(3000, function() {
    console.log('Server listening to port 3000.');
});