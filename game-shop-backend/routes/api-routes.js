let router = require('express').Router();

router.get('/', (req, res) => {
    res.json({
        status: '200'
    });
});

var gameController = require('../controllers/gameController');

router.route('/games')
    .get(gameController.index);

module.exports = router;