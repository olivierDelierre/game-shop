Game = require('../models/gameModel');

exports.index = function(req, res) {
    Game.get(function(err, games) {
        if (err) {
            res.json({
                status: "error",
                message: err
            });
        }

        res.json({
            status: "success",
            message: "Games retrieved successfully",
            data: games
        })
    });
}