var mongoose = require('mongoose');

var gameSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: String
});

var Game = module.exports = mongoose.model('game', gameSchema);

module.exports.get = function(callback, limit) {
    Game.find(callback).limit(limit);
}