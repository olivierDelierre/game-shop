import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Game } from './models/game';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  games: Game[] = [];

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>('/assets/games.json');
  }

  constructor(
    private http: HttpClient
  ) {}
}
