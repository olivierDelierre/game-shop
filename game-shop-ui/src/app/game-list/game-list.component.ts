import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { GameService } from '../game.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.sass']
})
export class GameListComponent implements OnInit {
  games: Observable<Game[]>;

  share() {
    window.alert('Vous avez bien partagé le jeu !');
  }

  onNotify() {
    window.alert('Vous serez prévenu quand ce jeu aura une promotion !');
  }

  constructor(
    private gameService: GameService
  ) { }

  ngOnInit(): void {
    this.games = this.gameService.getGames();
  }
}
