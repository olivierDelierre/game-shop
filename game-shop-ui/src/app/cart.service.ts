import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ShippingType } from './models/shippingType';
import { Game } from './models/game';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Game[] = [];

  addToCart(game: Game) {
    this.items.push(game);
  }

  getItems(): Game[] {
    return this.items;
  }

  clearCart(): Game[] {
    this.items = [];
    return this.items;
  }

  getShippingPrices(): Observable<ShippingType[]> {
    return this.http.get<ShippingType[]>('/assets/shipping.json');
  }

  constructor(
    private http: HttpClient
  ) {}
}
