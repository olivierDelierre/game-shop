import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { Game } from '../models/game';

import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {
  items: Game[];
  checkoutForm: FormGroup;
  shippingTypes;

  constructor(
    private cartService: CartService,
    private formBuilder: FormBuilder
  ) { 
    this.checkoutForm = this.formBuilder.group({
      name: ['', Validators.required],
      city: ['', Validators.required],
      shippingType: ['', Validators.required],
      zipCode: [''],
      street: [''],
      mail: ['', [Validators.required, Validators.email]]
    });
  }

  // Form getters
  get name() {
    return this.checkoutForm.get('name');
  }

  get city() {
    return this.checkoutForm.get('city');
  }

  ngOnInit(): void {
    this.items = this.cartService.getItems();
    this.shippingTypes = this.cartService.getShippingPrices();
  }

  onSubmit(customerData) {
    if (this.checkoutForm.invalid) {
      return;
    }

    console.warn('Order submitted', customerData);
    this.items = this.cartService.clearCart();
    this.checkoutForm.reset();
  }

  display() {
    console.log(this.checkoutForm);
  }
}
