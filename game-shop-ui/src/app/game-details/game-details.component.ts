import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Game } from '../models/game';

import { GameService } from '../game.service';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.sass']
})
export class GameDetailsComponent implements OnInit {
  game: Game;

  addToCart(game: Game) {
    this.cartService.addToCart(game);
    window.alert('Le jeu a été ajouté à votre panier !');
  }

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.gameService.getGames()
        .subscribe(response => {
          this.game = response[+params.get('gameId')];
        });
    });
  }
}
