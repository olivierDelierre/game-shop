import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-alerts',
  templateUrl: './game-alerts.component.html',
  styleUrls: ['./game-alerts.component.sass']
})
export class GameAlertsComponent implements OnInit {
  @Input() game;
  @Output() notify = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
