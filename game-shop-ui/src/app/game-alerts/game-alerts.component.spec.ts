import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameAlertsComponent } from './game-alerts.component';

describe('GameAlertsComponent', () => {
  let component: GameAlertsComponent;
  let fixture: ComponentFixture<GameAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
