export class Game {
    id: number;
    name: string;
    price: number;
    description: string;
}
