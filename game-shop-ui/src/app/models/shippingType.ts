export class ShippingType {
    id: number;
    name: string;
    price: number;
    needAddress: boolean;
}