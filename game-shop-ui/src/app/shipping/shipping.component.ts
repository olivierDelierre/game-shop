import { Component, OnInit } from '@angular/core';

import { CartService } from '../cart.service';
import { Observable } from 'rxjs';
import { ShippingType } from '../models/shippingType';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.sass']
})
export class ShippingComponent implements OnInit {
  shippingCosts: Observable<ShippingType[]>;

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.shippingCosts = this.cartService.getShippingPrices();
  }

}
